<?php

/*
	Page Name:
			php/languages/ko_KO.lang.php
	Description:
			Korean (ko_KO) language file for FPSDesigner
	Language:
			Korean - ko_KO
	Version:
			1.0.0
	Author:
*/

$lang_str["ko_KO"] = Array(

	/* Common */
	"CMN.LANG_HTMLID"		=> "ko",
	"CMN.LANG_TXT"			=> "한국어",
	"CMN.TITLE"				=> "FPS 디자이너",

	/* Meta */
	"CMN.META.AUTHOR"		=> "Adrien Neveu, David Peicho",
	"CMN.META.KEYWORDS"		=> "fps, 디자이너, fps디자이너, 생성, 비디오, 게임, 비디오게임, 쉬운, 디자인, 창작, 도구, 소프트웨어, epita, 일인칭, 시점, 슈터",
	"CMN.META.DESCRIPTION"	=> "FPS 디자이너 - 당신만의 FPS 게임을 만들수 있도록 해주는 최고의 도구.",

	"MENU.ABOUT"		=> "게임 소개",
	"MENU.FEATURES"		=> "기능들",
	"MENU.TEAM"			=> "팀 소개",
	"MENU.LANG"			=> "언어",

	"SIDEMENU.TITLE"	=> "FPS 디자이너",
	"SIDEMENU.1"		=> "무엇 인가요??",
	"SIDEMENU.2"		=> "쉬운 사용법",
	"SIDEMENU.3"		=> "환상적",
	"SIDEMENU.4"		=> "강력함",
	"SIDEMENU.5"		=> "고효율",
	"SIDEMENU.6"		=> "팀",

	"TEAM.1.TITLE"		=> "Adrien Neveu",
	"TEAM.1.EMAIL"		=> "adrien.neveu@epita.fr",
	"TEAM.1.TXT"		=> "해당 작업 담당: <strong>엔진 렌더링</strong>, <strong>멀티플레이어</strong> 그리고 <strong>소프트웨어 관련</strong> (<i>앞 과 뒤</i>), 이 프로젝트로 인해 게임을 만드는 법을 배울수 있었습니다. <strong>기초부터</strong> 필수적인 요소들을 배울수 있었습니다. <strong>비디오 게임</strong> 뿐만 아닌 모던 <strong>소프트웨어도 말이죠</strong>.",

	"TEAM.2.TITLE"		=> "David Peicho",
	"TEAM.2.EMAIL"		=> "david.peicho@epita.fr",
	"TEAM.2.TXT"		=> "이 프로젝트로 인해 3D 게임 개발과, 전체적인 게임 개발에 대하여 많이 배울수 있게 된, 좋은 기회 였습니다.. 해당 작업 담당: <strong>게임플레이</strong>, 사용된 <strong>물리 엔진</strong>, 사용된 <strong>골격 애니메이션</strong> 그리고 <strong>에셋 관리</strong>, 새로운 컨셉과, 개념들을 배울수 있었습니다.",

	"CONTENT.CAT1.TITLE"	=> "FPS 디자이너, 무엇 인가요?",
	"CONTENT.CAT1.TEXT"		=> "FPS 디자이너 는 강력 하지만 사용하기 쉬운, FPS 게임 개발 프로그램 입니다. <br/>프로그래밍 지식 및 개발 개념 없이도 만들수 있는 컨셉으로 만들어진 프로그램 입니다.",
	"CONTENT.CAT1.TEXT2"	=> "FPS 디자이너는 Epita Engineering School의 두 학생들 Adrien Neveu 과 David Peicho의 1학년 프로젝트로 만들어진 프로그램 입니다..<br/>프로젝트에 관한 추가정보를 여기서 확인하실수 있습니다.<a href='https://github.com/AdrienNeveu/FPSDesigner'>GitHub 페이지 입니다!</a><br/>",

	"CONTENT.FEATURES.1.TITLE"	=> "<strong>쉬운 사용법.</strong> FPS 디자이너는 사용하고 싶은 그 누구도 쉽게 사용할수 이도록 만들어 졌습니다. 숙련자에겐 더욱 접근하기 쉽도록 만들어 졌으며, FPS 디자이너의 인간공학적인 인터페이스로 인해 어떤 누구도 쉽게 접근할수 있습니다.",
	"CONTENT.FEATURES.2.TITLE"	=> "<strong>환상적.</strong> 쉐이더 및 그래픽 기능들은 잊으세요, FPS 디자이너는 전용 렌더링 엔진을 사용 합니다. 덕분에, 에셋 프로젝션, 드로잉, 그림자, 라이트 시스템 까지도 관리 하실 필요 없습니다.",
	"CONTENT.FEATURES.3.TITLE"	=> "<strong>강력함.</strong>FPS 디자이너 인젠은 여러 리소스를 관리할수 있도록 개설 되었습니다. 3D 매쉬, 사운드, 파티클 및 다양한 처리 도 문제 없습니다. 컴퓨터의 성능을 걱정하지 않고도 게임을 즐길수 있도록 합니다.... ",
	"CONTENT.FEATURES.4.TITLE"	=> "<strong>효율적.</strong> 기술적인 면에 대한 걱정없이 게임을 개발 하실수 있습니다. 당신만의 세계를 만들어 보십시오, 당신만의 시놉시스, 당신이 원하는 게임플레이, 그리고 완성후 당신의 게임을 즐기면 됩니다!",

	"FOOTER"		=> "Copyright - FPSDesigner - {DATE}",

);

?>

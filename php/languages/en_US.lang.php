<?php

/*
	Page Name:
			php/languages/en_US.lang.php
	Description:
			English (en_US) language file for FPSDesigner
	Language:
			English - en_US
	Version:
			1.0.0
	Author:
			Adrien Neveu <adrien.neveu@hotmail.fr>
			David Peicho <david.peicho@epita.fr>
*/

$lang_str["en_US"] = Array(

	/* Common */
	"CMN.LANG_HTMLID"		=> "en",
	"CMN.LANG_TXT"			=> "English",
	"CMN.TITLE"				=> "FPS Designer",

	/* Meta */
	"CMN.META.AUTHOR"		=> "Adrien Neveu, David Peicho",
	"CMN.META.KEYWORDS"		=> "fps, designer, fpsdesigner, create, video, games, videogames, easily, design, creation, tool, software, epita, first, person, shooters",
	"CMN.META.DESCRIPTION"	=> "FPS Designer - A simple and easy to use tool to create your own First Person Shooter.",

	"MENU.ABOUT"		=> "About",
	"MENU.FEATURES"		=> "Features",
	"MENU.TEAM"			=> "The Team",
	"MENU.LANG"			=> "Language",

	"SIDEMENU.TITLE"	=> "FPS Designer",
	"SIDEMENU.1"		=> "What's that?",
	"SIDEMENU.2"		=> "It's easy to use",
	"SIDEMENU.3"		=> "It's stunning",
	"SIDEMENU.4"		=> "It's powerful",
	"SIDEMENU.5"		=> "It's efficient",
	"SIDEMENU.6"		=> "The Team",

	"TEAM.1.TITLE"		=> "Adrien Neveu",
	"TEAM.1.EMAIL"		=> "adrien.neveu@epita.fr",
	"TEAM.1.TXT"		=> "In charge of the <strong>rendering engine</strong>, of the <strong>multiplayer</strong> and the <strong>software part</strong> (<i>front and back ends</i>), this project taught me how to create a project <strong>from scratch</strong> and the fundamentals of developing a <strong>video game</strong> and a modern <strong>software</strong>.",

	"TEAM.2.TITLE"		=> "David Peicho",
	"TEAM.2.EMAIL"		=> "david.peicho@epita.fr",
	"TEAM.2.TXT"		=> "This project has been an opportunity to improve our video games development skills in 3D video games. In charge of the <strong>Gameplay</strong>, the <strong>physicss engine</strong>, the <strong>skeletal animation</strong> and of the <strong>assets handling</strong>, I had the opportunity to discover new concepts, to learn notions that I did not know about.",

	"CONTENT.CAT1.TITLE"	=> "FPS Designer, what's that?",
	"CONTENT.CAT1.TEXT"		=> "FPS Designer is a software both powerful and easy to use, that allows you to create first person shooter. <br/>The concept is that it does not require neither programming skills nor an extraordinary budget.",
	"CONTENT.CAT1.TEXT2"	=> "FPS Designer has been developed as a first year project by two students from the Epita Engineering School, Adrien Neveu and David Peicho.<br/>Check out the project <a href='https://github.com/AdrienNeveu/FPSDesigner'>GitHub page here!</a><br/>",

	"CONTENT.FEATURES.1.TITLE"	=> "<strong>It's easy to use.</strong> FPS Designer was designed for anyone who wants to use it. Having some technical skills would be a benefit, but the FPS Designer ergonomic interface ensures to familiarize yourself with the software.",
	"CONTENT.FEATURES.2.TITLE"	=> "<strong>It's stunning.</strong> Forget about shaders or other graphical features, FPS Designer has its own rendering engine. Thanks to it, you will not care about assets projection, drawing, shadows or even the light system.",
	"CONTENT.FEATURES.3.TITLE"	=> "<strong>It's powerful.</strong> The FPS Designer engine was made in order to deal with multiple resources, such as 3D meshes, sounds, particles or even procedural trees. You do not need to be concerned about the computer performances to play your game... ",
	"CONTENT.FEATURES.4.TITLE"	=> "<strong>It's efficient.</strong> You do not have to be concerned about the technical aspects making up a game. Build your own world, with your own synopsis, based on the gameplay you want, and finally, your game is ready to be played!",

	"FOOTER"		=> "Copyright - FPSDesigner - {DATE}",

);

?>

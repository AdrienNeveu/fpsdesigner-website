<?php

/*
	Page Name:
			php/languages/fr_FR.lang.php
	Description:
			French (fr_FR) language file for FPSDesigner
	Language:
			French - fr_FR
	Version:
			1.0.0
	Author:
			Adrien Neveu <adrien.neveu@hotmail.fr>
			David Peicho <david.peicho@epita.fr>
*/

$lang_str["fr_FR"] = Array(

	/* Common */
	"CMN.LANG_HTMLID"		=> "fr",
	"CMN.LANG_TXT"			=> "Français",
	"CMN.TITLE"				=> "FPS Designer",

	/* Meta */
	"CMN.META.AUTHOR"		=> "Adrien Neveu, David Peicho",
	"CMN.META.KEYWORDS"		=> "fps, designer, fpsdesigner, create, video, games, videogames, easily, design, creation, tool, software, epita, first, person, shooters",
	"CMN.META.DESCRIPTION"	=> "FPS Designer - L'outil le plus simple pour créer vos jeux de tir à la première personne.",

	"MENU.ABOUT"		=> "A Propos",
	"MENU.FEATURES"		=> "Fonctionnalités",
	"MENU.TEAM"			=> "L'équipe",
	"MENU.LANG"			=> "Langue",

	"SIDEMENU.TITLE"	=> "FPS Designer",
	"SIDEMENU.1"		=> "C'est quoi ?",
	"SIDEMENU.2"		=> "C'est simple",
	"SIDEMENU.3"		=> "C'est beau",
	"SIDEMENU.4"		=> "C'est puissant",
	"SIDEMENU.5"		=> "C'est rapide",
	"SIDEMENU.6"		=> "L'équipe",

	"TEAM.1.TITLE"		=> "Adrien Neveu",
	"TEAM.1.EMAIL"		=> "adrien.neveu@epita.fr",
	"TEAM.1.TXT"		=> "En charge du <strong>moteur de rendu</strong>, du <strong>multijoueur</strong> et de la <strong>partie logicielle</strong> (<i>front et back ends</i>), ce projet m'a permis d'apprendre à réaliser <strong>un projet de A à Z</strong> et les fondamentaux du développement d'un <strong>jeu vidéo</strong> et d'un <strong>logiciel</strong> moderne.",

	"TEAM.2.TITLE"		=> "David Peicho",
	"TEAM.2.EMAIL"		=> "david.peicho@epita.fr",
	"TEAM.2.TXT"		=> "Ce projet de première année a été fructueux dans notre façons de développer des jeux vidéo 3D.<br/>En charge du <strong>Gameplay</strong>, de la <strong>physique</strong>, des <strong>nimations squelettales</strong> et de la <strong>gestion des ressources</strong>, j'ai pu découvrir de nouveaux concepts et appréhender des notions qui m'étaient jusqu'ici, inconnues.",

	"CONTENT.CAT1.TITLE"	=> "FPS Designer, c'est quoi ?",
	"CONTENT.CAT1.TEXT"		=> "FPS Designer est un logiciel à la fois puissant et simple à utiliser permettant de créer des jeux vidéo de tir à la première personne.<br/>Le concept de FPS Designer est de pouvoir créer un jeu vidéo sans connaissances techniques particulières ni d'un budget démesuré.",
	"CONTENT.CAT1.TEXT2"	=> "FPS Designer a été réalisé comme projet de première année par un groupe de deux étudiants de l'EPITA à Paris, Adrien Neveu et David Peicho.<br/>Jetez un oeil à la page GitHub du projet <a href='https://github.com/AdrienNeveu/FPSDesigner'>ici !</a><br/>",

	"CONTENT.FEATURES.1.TITLE"	=> "<strong>C'est simple.</strong> FPSDesigner a été conçu pour être utilisé par n'importe qui. Que vous ayez des compétences techniques ou si vous débutez complètement dans la création de jeu, l'interface ergonomique de FPSDesigner vous garantie une prise en main très rapide !",
	"CONTENT.FEATURES.2.TITLE"	=> "<strong>C'est beau.</strong> Oubliez la création de shaders ou autre aspects graphiques, un moteur de rendu a déjà été entièrement réalisé et est intégré dans FPS Designer. Grâce à notre moteur, vous n'aurez pas à vous soucier des détails du terrain, des ombres ou des effets lumineux...",
	"CONTENT.FEATURES.3.TITLE"	=> "<strong>C'est puissant.</strong> Le moteur de FPSDesigner a été réalisé de telle sorte qu'il puisse supporter des milliers d'objets, de textures, de sources lumineuses ou d'armes différentes. Ne vous souciez plus des ordinateurs peu performants pour jouer à votre jeu... ",
	"CONTENT.FEATURES.4.TITLE"	=> "<strong>C'est rapide.</strong> Vous n'avez plus à vous soucier de tous les détails techniques qu'un jeu peut posséder. Créez votre monde, votre scénario, votre gameplay et votre interface et votre jeu est terminé et prêt à être joué !",

	"FOOTER"		=> "Copyright - FPSDesigner - Adrien Neveu & David Peicho - {DATE}",

);

?>

<?php
	class Lang
    {
		private $lang = Array();
		private $lang_id = "";
		public $langDir = "languages/";

        public function __construct($language=false)
		{
			if($language === false)
			{
				if(isset($_COOKIE["WEBSITE_LANG_ID"]) && $this->isValidLang($_COOKIE["WEBSITE_LANG_ID"]))
					$language = $_COOKIE["WEBSITE_LANG_ID"];
				else
				{
					switch(substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2))
					{
						case "fr-fr":
						case "fr-be":
						case "fr":
							$language = "fr_FR";
							break;
						case "ko":
							$language = "ko_KO";
							break;
						default:
							$language = "en_US";
							break;
					}
				}
			}
			if($this->isValidLang($language))
			{
				setCookie("WEBSITE_LANG_ID",$language,time()+60*60*24*30,"/",'.'.$_SERVER['HTTP_HOST']);
				$_COOKIE["WEBSITE_LANG_ID"] = $language;
				$lang_str = Array();
				include("php/".$this->langDir.$language.'.lang.php');
				$this->lang = $lang_str[$language];
				$this->lang_id = $language;
			} else
				return false;
		}

		public function isValidLang($language)
		{
			return is_file("php/".$this->langDir.$language.".lang.php");
		}

		public function getLangId()
		{
			return $this->lang_id;
		}

		public function getString($str, $find=false, $replace=false)
		{
			if(!isset($this->lang[$str]))
				return "{".$str."}";
			if(!$find)
				return $this->lang[$str];
			else
				return str_replace($find,$replace,$this->lang[$str]);
		}

    }

?>

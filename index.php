<?php
require_once('php/classes/Lang.class.php');
if(isset($_GET["lang"]))
	$Lang = new Lang($_GET["lang"]);
else
	$Lang = new Lang();
?><!DOCTYPE html>
<!--[if IE 8]> <html lang="<?=$Lang->getString("CMN.LANG_HTMLID")?>" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="<?=$Lang->getString("CMN.LANG_HTMLID")?>" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="<?=$Lang->getString("CMN.LANG_HTMLID")?>"> <!--<![endif]-->
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, user-scalable=0">

	<title><?=$Lang->getString("CMN.TITLE")?></title>
	<script>
	document.ontouchstart = function(e){e.preventDefault();}
	</script>
	<meta name="description" content="<?=$Lang->getString("CMN.META.DESCRIPTION")?>">
	<meta name="author" content="<?=$Lang->getString("CMN.META.AUTHOR")?>">
	<meta name="keywords" content="<?=$Lang->getString("CMN.META.KEYWORDS")?>">

	<link rel="shortcut icon" href="favicon.ico">

	<!-- THEME STYLES -->
	<link rel="stylesheet" href="css/nav.css" type="text/css" />
	<link rel="stylesheet" href="css/style.css" type="text/css" />
	<link rel="stylesheet" href="css/responsive.css" type="text/css" />
	<link rel="stylesheet" href="css/custom.css" type="text/css" />
	<!-- PLUGIN STYLES -->
	<link rel="stylesheet" href="css/mobile-menu.css" media="all" type="text/css" />
	<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/owl.theme.css">
	<link rel="stylesheet" href="css/slidesJS-custom.css">
	<!-- Bootstrap -->
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap-responsive.css" type="text/css" />
	<!-- Font Awesome -->
	<link rel="stylesheet" href="font-awesome/css/font-awesome.css" type="text/css" />
	<!-- Open Sans Google Font -->
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<!--//-->
</head>

<body id="home">
	<!-- PRELOADER -->
	<div id="preloader">
		<div class="spinner"></div>
	</div>
	<!-- // -->
	<!-- HEADER -->
	<div id="wrap-header">
		<div class="SL_swap" id="main-header">
			<!-- Logo -->
			<div id="header-logo-wrap">
				<a href="#home" id="header-logo" class="dropdown-toggle disabled">FPS Designer</a>
			</div>
			<!--// -->
			<div class="right">
				<ul class="main-navigation">
					<li class="dropdown">
						<a href="#about" data-toggle="dropdown" class="dropdown-toggle disabled">
							<?=$Lang->getString("MENU.ABOUT")?>
						</a>
					</li>

					<li class="dropdown">
						<a href="#feature_1" data-toggle="dropdown" class="dropdown-toggle disabled">
							<?=$Lang->getString("MENU.FEATURES")?>
						</a>
					</li>

					<li class="dropdown">
						<a href="#team" data-toggle="dropdown" class="dropdown-toggle disabled">
							<?=$Lang->getString("MENU.TEAM")?>
						</a>
					</li>

					<li class="dropdown">
						<a href="#" data-toggle="dropdown" class="dropdown-toggle disabled">
							<?=$Lang->getString("MENU.LANG")?>
							<div class="hidden-hover"></div>
						</a>
						<ul id="downloads" class="dropdown-menu">
							<li><a class="nav-sprite-container" href="?lang=en_US"><i class="icon-sprite icon-arrow-right"></i>English</a></li>
							<li><a class="nav-sprite-container" href="?lang=fr_FR"><i class="icon-sprite icon-arrow-right"></i>Français</a></li>
							<li><a class="nav-sprite-container" href="?lang=ko_KO"><i class="icon-sprite icon-arrow-right"></i>한국어</a></li>
						</ul>
					 </li>
				</ul>
			</div>
		</div>
	</div>
	<!-- //HEADER -->

	<!-- MAIN WRAPPER -->
	<div id="wrap">
	<div id="home-content">

	<div id="sliderWrapper" class="homeshadow">
		<!-- Start Banner -->
		<div class="bannercontainer responsive">
			<div id="slides" class="banner">
				<iframe width="100%" height="100%" src="https://www.youtube.com/embed/KYwLaBxQztM?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
				<img src="images/slider/slider_nexus_black_2.jpg" alt="">
				<img src="images/slider/slider_iphone_black_1.jpg" alt="">
				<img src="images/slider/slider_iphone_black_2.jpg" alt="">
				<img src="images/slider/slider_imac_black_1.jpg" alt="">
				<a href="#" class="slidesjs-previous slidesjs-navigation"></a>
				<a href="#" class="slidesjs-next slidesjs-navigation"></a>
		  </div>
		</div>
		<!-- End Banner -->
	</div>

	<!-- Sticky Side Navigation -->
	<div class="page homeshadow subNavScroll" id="wrapper">
		<div class="subNav homeshadow">
			<ul class="subNavigation">
				<li>
					<span class="subNavtitle"><?=$Lang->getString("SIDEMENU.TITLE")?></span>
				</li>
				<li>
					<a href="#about">
						<i class="icon-menu-sprite icon-question-sign"></i>
						<span><?=$Lang->getString("SIDEMENU.1")?></span>
					</a>
				</li>
				<li>
					<a href="#feature_1">
						<i class="icon-menu-sprite icon-magic"></i>
						<span><?=$Lang->getString("SIDEMENU.2")?></span>
					</a>
				</li>
				<li>
					<a href="#feature_2">
						<i class="icon-menu-sprite icon-camera-retro"></i>
						<span><?=$Lang->getString("SIDEMENU.3")?></span>
					</a>
				</li>
				<li>
					<a href="#feature_3">
						<i class="icon-menu-sprite icon-signal"></i>
						<span><?=$Lang->getString("SIDEMENU.4")?></span>
					</a>
				</li>
				<li>
					<a href="#feature_4">
						<i class="icon-menu-sprite icon-dashboard"></i>
						<span><?=$Lang->getString("SIDEMENU.5")?></span>
					</a>
				</li>
				<li>
					<a href="#team">
						<i class="icon-menu-sprite icon-group"></i>
						<span><?=$Lang->getString("SIDEMENU.6")?></span>
					</a>
				</li>
			</ul>
		</div>
		<!-- //Sticky Side Navigation -->

		<div id="primary">
		<div id="content">

		<section class="home-block" id="about">
			<h2><?=$Lang->getString("CONTENT.CAT1.TITLE")?></h2>
			<h3 class="introParagraph"><?=$Lang->getString("CONTENT.CAT1.TEXT")?></h3>
			<ul class="showcaseThumbs owl-carousel gallery-item" id="thumbSlider">
				<li>
					<a href="images/gallery/large/1.jpg">
						<img class="img-polaroid img-rounded lazyOwl" data-src="images/gallery/thumbs/1.jpg" src="images/gallery/thumbs/6.jpg" alt="" >
					</a>
				</li>
				<li>
					<a href="images/gallery/large/2.png">
						<img class="img-polaroid img-rounded lazyOwl" data-src="images/gallery/thumbs/2.jpg" src="images/gallery/thumbs/2.jpg" alt="" >
					</a>
				</li>
				<li>
					<a href="images/gallery/large/3.jpg">
						<img class="img-polaroid img-rounded lazyOwl" data-src="images/gallery/thumbs/3.jpg" src="images/gallery/thumbs/3.jpg" alt="" >
					</a>
				</li>
				<li>
					<a href="images/gallery/large/4.png">
						<img class="img-polaroid img-rounded lazyOwl" data-src="images/gallery/thumbs/4.jpg" src="images/gallery/thumbs/4.jpg" alt="" >
					</a>
				</li>
				<li>
					<a href="images/gallery/large/5.jpg">
						<img class="img-polaroid img-rounded lazyOwl" data-src="images/gallery/thumbs/5.jpg" src="images/gallery/thumbs/5.jpg" alt="" >
					</a>
				</li>
				<li>
					<a href="images/gallery/large/6.jpg">
						<img class="img-polaroid img-rounded lazyOwl" data-src="images/gallery/thumbs/6.jpg" src="images/gallery/thumbs/1.jpg" alt="" >
					</a>
				</li>
			</ul>
			<p><?=$Lang->getString("CONTENT.CAT1.TEXT2")?></p>
		</section>

		<!-- START FEATURES 1 BLOCK -->
		<section class="home-block alternate no-padding-left" id="feature_1">
			<div class="row-fluid">
				<div class="span6">
					<div class="text-left">
						<ul>
							<li>
								<img class="no-padding-left minHeightImg" alt="" src="img/loader.gif" data-src="images/screenGallery/screen_1b_macbook.png">
							</li>
						</ul>
					</div>
				</div>
				<div class="span6">
					<div class="text-left intro">
						<!--<h4 class="scrollNav">Feature Title Here</h4>-->
						<p>
							<?=$Lang->getString("CONTENT.FEATURES.1.TITLE")?>
						</p>
						<div class="videobox gallery-item">
							<a href="https://www.youtube.com/watch?v=KYwLaBxQztM" class="mfp-iframe">
								<img class="img-polaroid img-rounded" src="images/gallery/thumbs/video_1.jpg" alt="">
								<span></span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- //END FEATURES 1 BLOCK -->

		<!-- START FEATURES 2 BLOCK -->
		<section class="home-block no-padding-left" id="feature_2">
			<div class="row-fluid">
				<div class="span6 pull-right no-padding-right">
					<div class="text-right">
						<ul>
							<li>
								<img class="no-padding-right minHeightImg" alt="" src="img/loader.gif" data-src="images/screenGallery/screen_1b_cinema.png">
							</li>
						</ul>
					</div>
				</div>
				<div class="span6">
					<div class="text-left intro_opp">
						<!--<h4 class="scrollNav">Feature Title Here</h4>-->
						<p>
							<?=$Lang->getString("CONTENT.FEATURES.2.TITLE")?>
						</p>
					</div>
				</div>
			</div>
		</section>
		<!-- //END FEATURES 2 BLOCK -->

		<!-- START FEATURES 3 BLOCK -->
		<section class="home-block alternate no-padding-left" id="feature_3">
			<div class="row-fluid">
				<div class="span6 pull-left no-padding-left">
					<div class="text-left">
						<ul>
							<li>
								<img class="no-padding-left minHeightImg" alt="" src="img/loader.gif" data-src="images/screenGallery/screen_1b_cinema_opp.png">
							</li>
						</ul>
					</div>
				</div>
				<div class="span6">
					<div class="text-left intro">
						<!--<h4 class="scrollNav">Feature Title Here</h4>-->
						<p>
							<?=$Lang->getString("CONTENT.FEATURES.3.TITLE")?>
						</p>
						<ul class="showcaseThumbs owl-carousel gallery-item" id="thumbSliderMini">
							<li>
								<a href="images/gallery/large/1.jpg">
									<img class="img-polaroid img-rounded lazyOwl" data-src="images/gallery/thumbs/1.jpg" src="images/gallery/thumbs/5.jpg" alt="" >
								</a>
							</li>
							<li>
								<a href="images/gallery/large/2.png">
									<img class="img-polaroid img-rounded lazyOwl" data-src="images/gallery/thumbs/2.jpg" src="images/gallery/thumbs/1.jpg" alt="" >
								</a>
							</li>
							<li>
								<a href="images/gallery/large/3.jpg">
									<img class="img-polaroid img-rounded lazyOwl" data-src="images/gallery/thumbs/3.jpg" src="images/gallery/thumbs/7.jpg" alt="" >
								</a>
							</li>
							<li>
								<a href="images/gallery/large/4.png">
									<img class="img-polaroid img-rounded lazyOwl" data-src="images/gallery/thumbs/4.jpg" src="images/gallery/thumbs/8.jpg" alt="" >
								</a>
							</li>
							<li>
								<a href="images/gallery/large/5.jpg">
									<img class="img-polaroid img-rounded lazyOwl" data-src="images/gallery/thumbs/5.jpg" src="images/gallery/thumbs/2.jpg" alt="" >
								</a>
							</li>
							<li>
								<a href="images/gallery/large/6.jpg">
									<img class="img-polaroid img-rounded lazyOwl" data-src="images/gallery/thumbs/6.jpg" src="images/gallery/thumbs/4.jpg" alt="" >
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<!-- //END FEATURES 3 BLOCK -->

		<!-- START FEATURES 4 BLOCK -->
		<section class="home-block no-padding-left" id="feature_4">
			<div class="row-fluid">
				<div class="span6 pull-right no-padding-right">
					<div class="text-right">
						<ul>
							<li>
								<img class="no-padding-right minHeightImg" alt="" src="img/loader.gif" data-src="images/screenGallery/screen_1b_cinema_opp2.png">
							</li>
						</ul>
					</div>
				</div>
				<div class="span6">
					<div class="text-left intro_opp">
						<!--<h4 class="scrollNav">Feature Title Here</h4>-->
						<p>
							<?=$Lang->getString("CONTENT.FEATURES.4.TITLE")?>
						</p>
					</div>
				</div>
			</div>
		</section>
		<!-- //END FEATURES 4 BLOCK -->

		<!-- START TEAM SECTION -->
		<section class="home-block block-team alternate" id="team">
			<div style="float:left;">
				<img src="images/team/adrienneveu.jpg"/><br/>
				<strong><?=$Lang->getString("TEAM.1.TITLE")?></strong><br/>
				<?=$Lang->getString("TEAM.1.EMAIL")?>
				<hr/>
				<?=$Lang->getString("TEAM.1.TXT")?>
			</div>
			<div style="float:right;">
				<img src="images/team/davidpeicho.jpg"/><br/>
				<strong><?=$Lang->getString("TEAM.2.TITLE")?></strong><br/>
				<?=$Lang->getString("TEAM.2.EMAIL")?>
				<hr />
				<?=$Lang->getString("TEAM.2.TXT")?>
			</div>
			<div style="clear:both; font-size:1px;"></div>
		</section>
		<!-- //END TEAM SECTION// -->

		<!-- START SOCIAL BLOCK -->
		<section class="home-block alternate" id="home-social">
			<div class="social row-fluid">
				<ul class="soc_list">
					<li>
						<a href="https://fr.linkedin.com/in/neveuadrien" class="disabled">
							<img alt="" onclick="window.location.href='https://fr.linkedin.com/in/neveuadrien'" src="img/social/linkedin.png">
						</a>
					</li>
					<li>
						<a href="https://github.com/AdrienNeveu/FPSDesigner/tree/master" class="disabled">
							<img alt="" onclick="window.location.href='https://github.com/AdrienNeveu/FPSDesigner/tree/master'" src="img/social/github.png">
						</a>
					</li>
					<li>
						<a href="https://www.youtube.com/channel/UCSUoDXW8cdNmWgjJ9oTNlQQ" class="disabled">
							<img alt="" onclick="window.location.href='https://www.youtube.com/channel/UCSUoDXW8cdNmWgjJ9oTNlQQ'" src="img/social/youtube.png">
						</a>
					</li>
					<li>
						<a href="#" class="disabled">
							<img alt="" src="img/social/linkedin.png">
						</a>
					</li>
				</ul>
			</div>
		</section>
		<!-- //END SOCIAL BLOCK -->
		<div class="clearfix"></div>
		</div>
		</div>
	</div>
	</div>
	</div>

	<!-- START FOOTER BLOCK -->
	<div class="clearfix"></div>
	<div id="footer">
		<div id="copyright">
			<ul>
				<li><?=$Lang->getString("MENU.LANG")?></li>
				<li>
					<a class="copyright" href="?lang=en_US">English</a>
				</li>
				<li>
					<a class="copyright" href="?lang=fr_FR">Français</a>
				</li>
			</ul>
			<ul>
				<li><?=$Lang->getString("FOOTER", "{DATE}", date("Y"))?></li>
			</ul>
		</div>
	</div>
	<!-- //END FOOTER BLOCK -->

	<!-- THEME JAVASCRIPT -->
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script type="text/javascript" src="js/init.js"></script>
	<script type="text/javascript" src="js/modernizr-2.0.6.min.js"></script>
	<!-- Mobile Menu -->
	<script type="text/javascript" src="js/mobile-menu.js"></script>
	<!-- Waypoints -->
	<script type="text/javascript" src="js/waypoints.js"></script>
	<!-- Scrollto -->
	<script type="text/javascript" src="js/jquery.localscroll-1.2.7-min.js"></script>
	<script type="text/javascript" src="js/jquery.scrollTo-1.4.3.1-min.js"></script>
	<!-- Magnific Popup -->
	<script type="text/javascript" src="js/jquery.magnific-popup.js"></script>
	<!-- Thumbnail Carousel JS -->
	<script type="text/javascript" src="js/owl.carousel.js"></script>
	<!-- Subscription JS -->
	<script type="text/javascript" src="js/subscribe.js"></script>
	<!-- SlidesJS -->
	<script type="text/javascript" src="js/jquery.slides.min.js"></script>
	<!-- RetinaJS -->
	<script type="text/javascript" src="js/retina.js"></script>
	<!-- BOOTSTRAP JS -->
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<!-- Preloader Init -->
	<script type="text/javascript">
		//<![CDATA[
		$(window).load(function() {
			"use strict";
			window.scrollTo(0, 1);
			$(".activeMenu").removeClass("activeMenu");
			$(".spinner").fadeOut();
			$("#preloader").delay(350).fadeOut("slow");
			$("body").delay(450).css("overflow", "auto");
			document.ontouchstart = function(e){return true;}
			$("li img").unveil(300, function() {
				$(this).load(function() {
					this.style.opacity = 1;
					$("li img").removeClass('minHeightImg')
				});
			});
			$(".subNav").sticky({ topSpacing: 56 });
		});
		//]]>
	</script>
</body>
</html>
